#pragma once
#ifndef __MOUSE__
#define __MOUSE__

#include "GameObject.h"
class CMouse : public CGameObject
{
public:
	explicit CMouse();
	virtual ~CMouse();

	// CGameObject을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;

	
private:
	bool IsClick;
	HDC hMemDC;
};

#endif // !__MOUSE__
