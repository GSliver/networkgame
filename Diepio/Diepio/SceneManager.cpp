#include "stdafx.h"
#include "SceneManager.h"
#include "Logo.h"
#include "Menu.h"
#include "WaitRoom.h"
#include "Stage.h"
IMPLEMENT_SINGLETON(CSceneManager)

CSceneManager::CSceneManager()
	:m_pScene(nullptr)
	,m_eCurScene(SCENE_END)
	,m_eNextScene(SCENE_END)
{
}

CSceneManager::~CSceneManager()
{
	Release();
}
void  CSceneManager::Update()
{
	if (m_pScene != nullptr)
		m_pScene->Update();
}


void CSceneManager::LateUpdate()
{
	if (m_pScene != nullptr)
		m_pScene->LateUpdate();
}

void CSceneManager::Render(HDC hDC)
{
	if (m_pScene != nullptr)
		m_pScene->Render(hDC);
}

void CSceneManager::SceneChange(SCENEID eID)
{
	m_eNextScene = eID; //��ȯ�� ��

	if (m_eNextScene != m_eCurScene)
	{
		Release(); //���� �� ����
		switch (m_eNextScene)
		{
		case SCENE_LOGO:
			m_pScene = new CLogo;
			break;
		case SCENE_MENU:
			m_pScene = new CMenu;
			break;
		case SCENE_WAITROOM:
			m_pScene = new CWaitRoom;
			break;
		case SCENE_STAGE:
			m_pScene = new CStage;
			break;
		case SCENE_END:
			break;
		default:
			break;
		}

		m_pScene->Initialize();
		m_eCurScene = m_eNextScene;
	}

}

void CSceneManager::Release()
{
	if (m_pScene != nullptr)
	{
		delete m_pScene;
		m_pScene = nullptr;
	}
}




