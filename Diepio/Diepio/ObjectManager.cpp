#include "stdafx.h"
#include "ObjectManager.h"
#include "GameObject.h"
#include "Player.h"
#include "Missile.h"
#include "GameManager.h"
IMPLEMENT_SINGLETON(CObjectManager)

CObjectManager::CObjectManager()
{

}

CObjectManager::~CObjectManager()
{
	Release();
}

void CObjectManager::AddObject(OBJECTTYPE eType, CGameObject* pObject)
{
	m_ObjList[eType].push_back(pObject);
}

void CObjectManager::Update()
{
	WaitForSingleObject(Bulletmutex, INFINITE);
	for (int i = 0; i < OBJECTTYPE::TYPE_END; ++i)
	{
		for (list<CGameObject*>::iterator iter = m_ObjList[i].begin(); iter != m_ObjList[i].end();)
		{
			int iEvent = 0;
			iEvent = (*iter)->Update();
			if (iEvent == OBJ_DEAD) {
				delete (*iter);
				(*iter) = nullptr;
				iter = m_ObjList[i].erase(iter);
			}
			else
				++iter;
		}
	}
	ReleaseMutex(Bulletmutex);
}

void CObjectManager::LateUpdate()
{
	for (int i = 0; i < OBJECTTYPE::TYPE_END; ++i)
	{
		for (auto& pObj : m_ObjList[i])
		{
			pObj->LateUpdate();
			if (m_ObjList[i].empty())
				break;
		}
	}
}

void CObjectManager::Render(HDC hDC)
{
	for (int i = 0; i < OBJECTTYPE::TYPE_END; ++i)
	{
		for (auto& pObj : m_ObjList[i])
		{
			pObj->Render(hDC);
			if (m_ObjList[i].empty())
				break;
		}
	}
}

void CObjectManager::Release()
{
	for (int i = 0; i < OBJECTTYPE::TYPE_END; ++i)
	{
		for (auto& pObj : m_ObjList[i])
		{
			if (nullptr != pObj) {
				delete pObj;
				pObj = nullptr;
			}
		}
		m_ObjList[i].clear();
	}
}

void CObjectManager::Delete_ID(OBJECTTYPE id)
{
	for (auto& pObj : m_ObjList[id]) {
		if (nullptr != pObj) {
			delete pObj;
			pObj = nullptr;
		}
	}
	m_ObjList[id].clear();
}

void CObjectManager::DeletebyIndex(OBJECTTYPE id, int index)
{
	int i = 0;
	for (list<CGameObject*>::iterator iter = m_ObjList[id].begin(); iter != m_ObjList[id].end();)
	{
		if(i++ == index){
			delete (*iter);
			(*iter) = nullptr;
			iter = m_ObjList[id].erase(iter);
			return;
		}
		else 
			++iter;
	}
}

void CObjectManager::PlayerUpdate(PlayerInfo pInfo)
{
	if (pInfo.pIndex < 0 || pInfo.pIndex>3)
		return;
	for (auto& player : m_ObjList[PLAYER])
	{
		if (pInfo.pIndex == dynamic_cast<CPlayer*>(player)->GetServerIndex())
		{
			dynamic_cast<CPlayer*>(player)->SetHp(pInfo.nowHp);
			dynamic_cast<CPlayer*>(player)->SetPosition(pInfo.x, pInfo.y);
			dynamic_cast<CPlayer*>(player)->SetGunPosition(pInfo.gunx1, pInfo.guny1, pInfo.gunx2, pInfo.guny2);
		}
	}
}

void CObjectManager::BulletUpdate(BulletInfo bInfo)
{
	WaitForSingleObject(Bulletmutex, INFINITE);
	for (auto& bullet : m_ObjList[MISSILE])
	{
		if (bInfo.bindex == dynamic_cast<CMissile*>(bullet)->Get_Index())
		{
			if (bInfo.isDead)
				bullet->Set_IsDead(true);
			else
				dynamic_cast<CMissile*>(bullet)->Set_Position(bInfo.x, bInfo.y);
			ReleaseMutex(Bulletmutex);
			return;
		}
	}
	ReleaseMutex(Bulletmutex);

	//for (list<CGameObject*>::iterator bullet = m_ObjList[MISSILE].begin(); bullet != m_ObjList[MISSILE].end();)
	//{
	//	if (bInfo.bindex == index++)
	//	{
	//		if (bInfo.isDead) {
	//			delete (*bullet);
	//			(*bullet) = nullptr;
	//			bullet = m_ObjList[MISSILE].erase(bullet);
	//		}
	//		else
	//			dynamic_cast<CMissile*>(*bullet)->Set_Position(bInfo.x, bInfo.y);
	//		SetEvent(g_BulletUpdate);
	//		return;
	//	}
	//	else
	//		++bullet;
	//}
}

void CObjectManager::Send_BulletInfo()
{
	for (auto& bullet : m_ObjList[MISSILE])
	{
		BulletInfo b;
		CMissile* m = dynamic_cast<CMissile*>(bullet);
		b.bindex = m->Get_Index();
		b.dx = m->Get_Direction().dx;
		b.dy = m->Get_Direction().dy;
		b.pIndex = m->Get_PlayerId();
		b.x = m->Get_Info()->fx;
		b.y = m->Get_Info()->fy;
		b.isDead = false;
		CGameManager::GetInstance()->SendData(BULLETINFO, &b);
	}
}

list<CGameObject*> CObjectManager::GetObjectList(OBJECTTYPE eType)
{
	return m_ObjList[eType];
}

CGameObject* CObjectManager::GetObjectbyindex(OBJECTTYPE eType, int index)
{
	list<CGameObject*>::iterator iter = m_ObjList[eType].begin();

	for (int i = 0; i < index; ++i)
	{
		++iter;
	}
	return (*iter);
}

const int& CObjectManager::GetWinnerIndex()
{
	for (auto& player : m_ObjList[PLAYER])
	{
		if (dynamic_cast<CPlayer*>(player)->GetCurrentHP() > 0.f)
			return dynamic_cast<CPlayer*>(player)->GetServerIndex();
	}

	return -1;
}
