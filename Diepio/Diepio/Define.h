#pragma once
#ifndef __DEFINE_H__
#define __DEFINE_H__

extern HWND g_hWnd;
extern HANDLE Bulletmutex;

#define NO_COPY(ClassName)						\
private:										\
	ClassName(const ClassName& Obj);			\
	ClassName& operator=(const ClassName& Obj);

#define DECLARE_SINGLETON(ClassName)		\
		NO_COPY(ClassName)					\
public:										\
	static ClassName* GetInstance()			\
	{										\
		if(nullptr == m_pInstance)			\
		{									\
			m_pInstance = new ClassName;	\
		}									\
		return m_pInstance;					\
	}										\
	void DestroyInstance()					\
	{										\
		if(m_pInstance)						\
		{									\
			delete m_pInstance;				\
			m_pInstance = nullptr;			\
		}									\
	}										\
private:									\
	static ClassName*	m_pInstance;

#define IMPLEMENT_SINGLETON(ClassName)		\
ClassName* ClassName::m_pInstance = nullptr;

#define WINCX 1280
#define WINCY 720
#define OBJ_DEAD 1
#define OBJ_NOEVENT 0


enum SCENEID{SCENE_LOGO, SCENE_MENU, SCENE_WAITROOM, SCENE_STAGE,SCENE_END};
enum OBJECTTYPE { PLAYER, MONSTER, MISSILE, OBSTACLE, BUTTON, MOUSE, TYPE_END};

typedef struct Information {
	Information() {	}
	Information(float x, float y, float xsize, float ysize)
		:fx(x), fy(y), fsizex(xsize), fsizey(ysize)
	{}
	float fx;
	float fy;
	float fsizex;
	float fsizey;
	float fspeed;
}INFO;

typedef struct NickNameInfo {
	char pIndex; //�÷��̾ �����ϱ� ���� �ε���
	char name[30]; //�÷��̾��� �г���
};

typedef struct Direction {
	float dx;
	float dy;
}DIR;

//////����//////

//�÷��̾� ���� ����ü
typedef struct PlayerInfo {
	char pIndex;		//�÷��̾ �����ϱ� ���� �ε���
	float x;			//�÷��̾��� X��ǥ
	float y;			//�÷��̾��� Y��ǥ
	char nowHp;			//�÷��̾��� ���� ü��
	char maxHp;			//�÷��̾��� �ִ� ü��
	float gunx1;		//�ѱ��� �� X��ǥ 1
	float guny1;		//�ѱ��� �� Y��ǥ 1
	float gunx2;		//�ѱ��� �� X��ǥ 2
	float guny2;		//�ѱ��� �� Y��ǥ 2
}USD;

//Ű�Է�
typedef struct KeyInfo {
	char pIndex; //�÷��̾ �����ϱ� ���� �ε���
	bool w;	//w �̵�
	bool a; //a �̵�
	bool s; //s �̵�
	bool d; //d �̵�
};
//���콺
typedef struct MouseInfo {
	char pIndex;      //�÷��̾ �����ϱ� ���� �ε���
	float mx;         //���콺 x��ǥ
	float my;         //���콺 y��ǥ
	bool isClicked;      //���콺 Ŭ�� ����(��ź �߻�)
};

//��ź ���� ����ü
typedef struct BulletInfo {
	char pIndex;      //�÷��̾ �����ϱ� ���� �ε���
	char bindex;      //��ź�� �����ϱ� ���� �ε���
	bool isDead;
	float x;         //��ź�� X��ǥ
	float y;         //��ź�� Y��ǥ
	float dx;		 //��ź�� x���� ����
	float dy;		 //��ź�� y���� ����
};

//��ֹ� ���� ����ü
typedef struct ObstacleInfo {
	float x;         //��ֹ��� �»�� X��ǥ
	float y;         //��ֹ��� �»�� Y��ǥ
};
#define KEYINFO 0
#define MOUSEINFO 1
#define PLAYERINFO 2
#define BULLETCREATE 3
#define BULLETINFO 4
#define OBSTACLEINFO 5
#define GAMESTATE 6
#define PLAYERINDEX 7
#define PLAYERACCEPT 8

typedef struct DataInfo {
	char infoindex;
	char datasize;
};

#endif // !__DEFINE_H__
