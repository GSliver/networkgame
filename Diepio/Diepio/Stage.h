#pragma once
#pragma once
#ifndef __STAGE__
#define __STAGE__
#include "Scene.h"

class CStage : public CScene
{
public:
	explicit CStage();
	virtual ~CStage();

	// CScene을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hdc) override;
	virtual void Release() override;

private:
	void RenderWinnerPlayer(HDC hdc);
private:
	float bulletcooltime = 0.f;
};

#endif