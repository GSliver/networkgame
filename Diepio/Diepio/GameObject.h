#pragma once
#ifndef __GAMEOBJECT__
#define __GAMEOBJECT__
#include "Define.h"

class CGameObject
{
public:
	explicit CGameObject();
	virtual ~CGameObject();

	virtual void Initialize() = 0;
	virtual int  Update() = 0;
	virtual void LateUpdate() = 0;
	virtual void Render(HDC hDC) = 0;
	virtual void Release() = 0;

public:
	//get
	OBJECTTYPE Get_ObjectId() { return m_eObjectID; }
	const RECT* Get_Rect() const { return &m_Rect; }
	const INFO* Get_Info() const { return &m_Info; }
	const bool& Get_IsDead() const { return m_IsDead; }
	//set
	void Set_Position(float x, float y) { m_Info.fx = x; m_Info.fy = y; }
	void Set_ObjectId(OBJECTTYPE eID) { m_eObjectID = eID; }
	void Set_IsDead(bool status) { m_IsDead = status; }
	void Set_Size(float sx, float sy) { m_Info.fsizex = sx; m_Info.fsizey = sy; }

public:
	void UpdateRect();

protected:
	OBJECTTYPE m_eObjectID;
	INFO m_Info; //객체 정보
	RECT m_Rect; //객체 충돌체

	bool m_IsDead = false; //생존 여부
};

#endif
