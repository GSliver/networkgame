#pragma once
#ifndef __BITMAPMGR__
#define __BITMAPMGR__

class CBitmapResource;

class CBitmapManager
{
private:
	CBitmapManager();
	~CBitmapManager();
public:
	static CBitmapManager* GetInstance()
	{
		if (nullptr == m_pInstance)
			m_pInstance = new CBitmapManager;
		return m_pInstance;
	}
	static void DestroyInstance() {
		if (m_pInstance) {
			delete m_pInstance;
			m_pInstance = nullptr;
		}
	}
public:
	void Release();
	HDC FindImage(const TCHAR* pImgKey);
	void InsertBmp(const TCHAR* pFilePath, const TCHAR* pImgKey);

private:
	map<const TCHAR*, CBitmapResource*> m_BmpMap;
	static CBitmapManager* m_pInstance;
};
#endif // !__BITMAPMGR__


