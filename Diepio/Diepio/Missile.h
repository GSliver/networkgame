#pragma once
#ifndef __MISSILE__
#define __MISSILE__

#include "GameObject.h"

class CMissile : public CGameObject
{
public:
	explicit CMissile();
	virtual ~CMissile();

public:
	// CGameObject을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;

public:
	//set
	void Set_Direction(float dx, float dy) { m_Dir.dx = dx; m_Dir.dy = dy; }
	void Set_PlayerId(char id) { playerId = id; }
	void Set_index(char index) { bindex = index; }
	//get
	const DIR& Get_Direction() { return m_Dir; }
	const char& Get_PlayerId() { return playerId; }
	const char& Get_Index() { return bindex; }
private:
	DIR m_Dir; //총알 방향
	float fSpeed; //속도
	char playerId; //플레이어 아이디
	char bindex = -1;
};

#endif // !__MISSILE__
