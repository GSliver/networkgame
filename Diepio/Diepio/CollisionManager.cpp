#include "stdafx.h"
#include "GameObject.h"
#include "CollisionManager.h"

CCollisionManager::CCollisionManager()
{
}

CCollisionManager::~CCollisionManager()
{
}

bool CCollisionManager::CollisionbyObjects(CGameObject* obj, CGameObject* obj2)
{
	RECT rc = {};
	if (IntersectRect(&rc, obj->Get_Rect(), obj2->Get_Rect()))
		return true;
	return false;
}
