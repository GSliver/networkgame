#include "stdafx.h"
#include "Stage.h"
#include "BitmapManager.h"
#include "SceneManager.h"
#include "ObjectManager.h"
#include "AbstractFactory.h"
#include "GameManager.h"
#include "TimerManager.h"
CStage::CStage()
{
}

CStage::~CStage()
{
}

void CStage::Initialize()
{
	CBitmapManager::GetInstance()->InsertBmp(L"../Resources/BackGround/Background.bmp", L"Background");
	CBitmapManager::GetInstance()->InsertBmp(L"../Resources/UI/WinnerPlayer1.bmp", L"WinnerPlayer1");
	CBitmapManager::GetInstance()->InsertBmp(L"../Resources/UI/WinnerPlayer2.bmp", L"WinnerPlayer2");
	CBitmapManager::GetInstance()->InsertBmp(L"../Resources/UI/WinnerPlayer3.bmp", L"WinnerPlayer3");

	CGameManager::GetInstance()->InitializeLivePlayerNum();
}

void CStage::Update()
{
	if (CGameManager::GetInstance()->GetLivePlayerNum() == 1) //게임 종료
		return;
	CObjectManager::GetInstance()->Update();
}

void CStage::LateUpdate()
{
	if (CGameManager::GetInstance()->GetLivePlayerNum() == 1) //게임 종료
		return;
	CObjectManager::GetInstance()->LateUpdate();
}

void CStage::Render(HDC hdc)
{
	//그리기
	HDC hBack = CBitmapManager::GetInstance()->FindImage(L"BackBuffer");//더블버퍼
	HDC hMemDC = CBitmapManager::GetInstance()->FindImage(L"Background");//배경
	BitBlt(hBack, 0, 0, 1280, 720, hMemDC, 0, 0, SRCCOPY);

	CObjectManager::GetInstance()->Render(hBack);

	//승리자 그리기
	if (CGameManager::GetInstance()->GetLivePlayerNum() == 1)
	{
		RenderWinnerPlayer(hBack);
	}

	BitBlt(hdc, 0, 0, 1280, 720, hBack, 0, 0, SRCCOPY);

	if (CGameManager::GetInstance()->GetLivePlayerNum() == 1) //게임 종료
		return;

	if (CGameManager::GetInstance()->GetPlayerServerID() == 0) {
		bulletcooltime += CTimerManager::GetInstance()->GetDelta();
		if (bulletcooltime > 0.025f) {
			CObjectManager::GetInstance()->Send_BulletInfo();
			bulletcooltime = 0.f;
		}
	}
}

void CStage::Release()
{
}

void CStage::RenderWinnerPlayer(HDC hdc)
{
	HDC hMemDC = NULL;
	//winner그리기
	switch (CGameManager::GetInstance()->GetWinnderIndex())
	{
	case 0: //player1 win
	{
		hMemDC = CBitmapManager::GetInstance()->FindImage(L"WinnerPlayer1");//배경
	}
	break;
	case 1: //playwe2 win
	{
		hMemDC = CBitmapManager::GetInstance()->FindImage(L"WinnerPlayer2");//배경
	}
	break;
	case 2: //player3 win
	{
		hMemDC = CBitmapManager::GetInstance()->FindImage(L"WinnerPlayer3");//배경
	}
	break;
	default:
		break;
	}

	BitBlt(hdc, 145, 175, 1000, 359, hMemDC, 0, 0, SRCCOPY);

}
