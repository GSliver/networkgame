#include "stdafx.h"
#include "Logo.h"
#include "TimerManager.h"
#include "BitmapManager.h"
#include "SceneManager.h"
CLogo::CLogo()
{
}

CLogo::~CLogo()
{
}

void CLogo::Initialize()
{
	fTime = 0.f;
	
	ShowCursor(false);
	//리소스 로드
	CBitmapManager::GetInstance()->InsertBmp(L"../Resources/BackGround/Loading.bmp", L"Logo");
}

void CLogo::Update()
{
	fTime += CTimerManager::GetInstance()->GetDelta();

	if (fTime > 3.f)
	{
		//Scene 전환 -> Menu
		CSceneManager::GetInstance()->SceneChange(SCENEID::SCENE_MENU);
		fTime = 0.f;
	}
}

void CLogo::LateUpdate()
{
}

void CLogo::Render(HDC hdc)
{
	HDC hMemDC = CBitmapManager::GetInstance()->FindImage(L"Logo");//로고이미지
	BitBlt(hdc, 0, 0, 1280, 720, hMemDC, 0, 0, SRCCOPY);

}

void CLogo::Release()
{
}
