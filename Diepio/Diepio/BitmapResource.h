#pragma once
#ifndef __MYBITMAP__
#define __MYBITMAP__


class CBitmapResource
{
public:
	explicit CBitmapResource();
	virtual ~CBitmapResource();

public:
	HDC Get_MemDC() { return m_hMemDC; }
	void LoadBmp(const TCHAR* pFilePath);
	void Release();
private:
	HDC m_hMemDC;
	HBITMAP m_hBitMap;
	HBITMAP m_hOldBitMap;

};
#endif // !__MYBITMAP__

