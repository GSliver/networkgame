#include "stdafx.h"
#include "KeyMgr.h"

CKeyManager* CKeyManager::m_pInstance = nullptr;


CKeyManager::CKeyManager()
{
	ZeroMemory(m_bKeyState, sizeof(m_bKeyState));
}

CKeyManager::~CKeyManager()
{
}

bool CKeyManager::KeyPressing(int iKey)
{
	//키상태 
	//0x0000 - 현재 x 이전도 x
	//0x0001 - 현재 x 이전에 o
	//0x8000 - 현재 o 이전 ㄴㄴ
	//0x8001 - 현재 o 이전 o
	if (GetAsyncKeyState(iKey) & 0x8001)
	{
		return true;
	}
	return false;
}

bool CKeyManager::KeyDown(int iKey)
{
	if (!m_bKeyState[iKey] && (GetAsyncKeyState(iKey) & 0x8000))
	{
		m_bKeyState[iKey] = !m_bKeyState[iKey];
		return true;
	}
	return false;
}

bool CKeyManager::KeyUp(int iKey)
{
	if (m_bKeyState[iKey] && !(GetAsyncKeyState(iKey) & 0x8000))
	{
		m_bKeyState[iKey] = !m_bKeyState[iKey];
		return true;
	}
	return false;
}

void CKeyManager::KeyUpdate()
{
	for (int i = 0; i < VK_MAX; ++i)
	{
		if (m_bKeyState[i] && !(GetAsyncKeyState(i) & 0x8000))
			m_bKeyState[i] = !m_bKeyState[i];
		if (!m_bKeyState[i] && (GetAsyncKeyState(i) & 0x8000))
			m_bKeyState[i] = !m_bKeyState[i];
	}
}
