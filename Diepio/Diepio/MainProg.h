#pragma once
#ifndef __MAINPROG__
#define __MAINPROG__
class CTimerManager;

class CMainProg
{
public:
	explicit CMainProg();
	virtual ~CMainProg();
public:
	void Initialize();
	void Update();
	void LateUpdate();
	void Render();
	void Release();
private:
	HDC m_hDC;

	//fps 
	TCHAR m_szFps[128];
	int m_iFps;
	float m_fTimeCount;

	//Manager
	CTimerManager* m_TimeMgr; //Ÿ�̸�

};

#endif // !__MAINPROG__
