#pragma once
#ifndef __PLAYER__
#define __PLAYER__

#include "GameObject.h"

class CPlayer : public CGameObject
{
public:
	explicit CPlayer();
	virtual ~CPlayer();

public:
	// CGameObject을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;

public:
	//set
	void SetNickname(const string& name) { nickname = name; }
	void SetPosition(const float& x, const float& y) { m_Info.fx = x; m_Info.fy = y; }
	void SetHp(const float& hp) { m_fHp = hp; }
	void SetServerIndex(const char& index) {
		m_serverindex = index;
		mouse.pIndex = index;
	}
	void SetGunPosition(const float& x1, const float& y1, const float& x2, const float& y2) {
		gunx1 = x1;
		guny1 = y1;
		gunx2 = x2;
		guny2 = y2;
	}

	//get
	const string& GetNickname() { return nickname; }
	const INFO& GetInfo() { return m_Info; }
	const int& GetServerIndex() { return m_serverindex; }
	const float& GetCurrentHP() { return m_fHp; }
private:
	void KeyInput();

private:
	float m_fHp; //현재체력
	float m_fMaxHP; // 최대 체력
	float m_fAtk; //플레이어 공격력
	float m_fSpeed = 100.f;


	HBRUSH hBrush, oldBrush; // 색 나타내기용
	HPEN hPen, oldPen; // 색 나타내기용

	float gunx1;		//총구의 점 X좌표 1
	float guny1;		//총구의 점 Y좌표 1
	float gunx2;		//총구의 점 X좌표 2
	float guny2;		//총구의 점 Y좌표 2
	string nickname; // 플레이어 이름
	char m_serverindex = -1; // 서버상의 인덱스

	float MouseupdateDelta = 0.f;

	MouseInfo mouse;

};

#endif // !__PLAYER__
