#pragma once

#ifndef __TRAP__
#define __TRAP__

#include "GameObject.h"

class CObstacle : public CGameObject
{
public:
	explicit CObstacle();
	virtual ~CObstacle();

	// CGameObject을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;

private:

};

#endif // !__TRAP__
