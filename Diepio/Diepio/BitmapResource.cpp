#include "stdafx.h"
#include "BitmapResource.h"
#include "Define.h"

CBitmapResource::CBitmapResource()
{
}

CBitmapResource::~CBitmapResource()
{
}

void CBitmapResource::LoadBmp(const TCHAR* pFilePath)
{
	HDC hDC = GetDC(g_hWnd);
	m_hMemDC = CreateCompatibleDC(hDC);
	ReleaseDC(g_hWnd, hDC);

	m_hBitMap = (HBITMAP)LoadImage(NULL, pFilePath,IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);
	m_hOldBitMap = (HBITMAP)SelectObject(m_hMemDC, m_hBitMap);
}

void CBitmapResource::Release()
{
	SelectObject(m_hMemDC, m_hOldBitMap);
	DeleteObject(m_hMemDC);

	DeleteDC(m_hMemDC);
}
