#pragma once
#ifndef __OBJMGR__
#define __OBJMGR__
#include "Define.h"
class CGameObject;
class CObjectManager
{
	DECLARE_SINGLETON(CObjectManager)

private:
	CObjectManager();
	~CObjectManager();

public:
	void AddObject(OBJECTTYPE eType, CGameObject* pObject);
public:
	void Update();
	void LateUpdate();
	void Render(HDC hDC);
	void Release();

	//delete
	void Delete_ID(OBJECTTYPE id);
	void DeletebyIndex(OBJECTTYPE id, int index);

	void PlayerUpdate(PlayerInfo pInfo);
	void BulletUpdate(BulletInfo bInfo);

	//missile
	void Send_BulletInfo();
public:
	//get
	list<CGameObject*> GetObjectList(OBJECTTYPE eType);
	CGameObject* GetObjectbyindex(OBJECTTYPE eType, int index);
	const int& GetWinnerIndex();


public:
	//void Load_MissileStatusFromServer();
	//void Load_PlayerStatusFromServer();
	//void Load_MonsterStatusFromServer();

private:
	static CObjectManager* m_Instance;
	// 반복자 패턴
	list<CGameObject*> m_ObjList[OBJECTTYPE::TYPE_END];
};

#endif // !__OBJMGR__
