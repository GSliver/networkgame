#pragma once
#ifndef __LOGO__
#define __LOGO__

#include "Scene.h"

class CLogo : public CScene
{
public:
	CLogo();
	virtual ~CLogo();

	// CScene을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hdc) override;
	virtual void Release() override;

private:
	//3초후 로고 넘어가는 용도.
	float fTime;

};

#endif // !__LOGO__
