#pragma once
#ifndef __KEYMANAGER_H__
#define __KEYMANAGER_H__
#define VK_MAX 0xff

class CKeyManager
{
public:
	static CKeyManager* GetInstance()
	{
		if (nullptr == m_pInstance)
			m_pInstance = new CKeyManager;

		return m_pInstance;
	}
	static void DestroyInstance()
	{
		if (m_pInstance)
		{
			delete m_pInstance;
			m_pInstance = nullptr;
		}
	}
private:
	CKeyManager();
	~CKeyManager();
public:
	bool KeyPressing(int iKey);
	bool KeyDown(int iKey);
	bool KeyUp(int iKey);
	void KeyUpdate();

private:
	static CKeyManager* m_pInstance;
	bool m_bKeyState[VK_MAX];
};
#endif