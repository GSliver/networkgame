#pragma once
#ifndef __SCENE__
#define __SCENE__

class CScene
{
public:
	explicit CScene();
	virtual ~CScene();

	virtual void Initialize() = 0;
	virtual void Update() = 0;
	virtual void LateUpdate() = 0;
	virtual void Render(HDC hdc) = 0;
	virtual void Release() = 0;
};

#endif // !__SCENE__
