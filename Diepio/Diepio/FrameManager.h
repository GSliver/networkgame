#pragma once
#ifndef __FRAMEMGR__
#define __FRAMEMGR__
#include "Define.h"

class CFrameManager
{
	DECLARE_SINGLETON(CFrameManager);

private:
	explicit CFrameManager();
	virtual ~CFrameManager();

public:
	bool FrameLimit(float fLimit);

private:
	LARGE_INTEGER m_CurTime;
	LARGE_INTEGER m_OldTime;
	LARGE_INTEGER m_CpuTick;
	float m_fTimeCount;

};
#endif

