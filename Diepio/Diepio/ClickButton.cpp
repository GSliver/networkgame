#include "stdafx.h"
#include "ClickButton.h"
#include "KeyMgr.h"
#include "SceneManager.h"
#include "Menu.h"
#include "BitmapManager.h"
#include "CollisionManager.h"
#include "ObjectManager.h"
CClickButton::CClickButton()
{
}

CClickButton::~CClickButton()
{
}

void CClickButton::Initialize()
{
	m_IsClicked = false;
}

int CClickButton::Update()
{

	return OBJ_NOEVENT;
}

void CClickButton::LateUpdate()
{
	if (m_eButtonType == BUTTONTYPE::LOBBY)
	{
		if (CCollisionManager::CollisionbyObjects(this, CObjectManager::GetInstance()->GetObjectList(MOUSE).front())) //마우스와 충돌중
		{
			if (CKeyManager::GetInstance()->KeyPressing(VK_LBUTTON))
			{
				//서버에 닉네임 전송
				//중복 검사 후 결과 송신
				//플레이어에게 닉네임 대입, 플레이어 인덱스 대입
				m_IsClicked = true;
			}
			else
				m_IsClicked = false;
		}
		else
			m_IsClicked = false;
	}
}

void CClickButton::Render(HDC hDC)
{
	CGameObject::UpdateRect();
	HDC hMemDC = NULL;
	switch (m_eButtonType)
	{
	case LOBBY:
		hMemDC = CBitmapManager::GetInstance()->FindImage(L"StartButton");
		break;
	case READY:
		hMemDC = CBitmapManager::GetInstance()->FindImage(L"ReadyState");
		break;
	case UNREADY:
		break;
	case BUTTON_END:
		break;
	default:
		break;
	}

	if (hMemDC) {

		GdiTransparentBlt(hDC,
			(int)m_Rect.left, (int)m_Rect.top,
			(int)m_Info.fsizex, (int)m_Info.fsizey,
			hMemDC, 0, 0,
			(int)m_Info.fsizex, (int)m_Info.fsizey,
			RGB(255, 255, 255));
	}
}

void CClickButton::Release()
{
}
