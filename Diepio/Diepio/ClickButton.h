#pragma once
#ifndef __STRBUTTON__
#define __STRBUTTON__

#include "GameObject.h"

enum BUTTONTYPE {
	LOBBY, UNREADY, READY , BUTTON_END
};

class CClickButton : public CGameObject
{
public:
	explicit CClickButton();
	virtual ~CClickButton();

public:
	// CGameObject을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;

	//get
	bool Get_IsClicked() {return m_IsClicked; }
	//set
	void SetButtonType(BUTTONTYPE eType) { m_eButtonType = eType; }

private:
	bool m_IsClicked;
	BUTTONTYPE m_eButtonType = BUTTON_END;
};

#endif // !__STRBUTTON__
