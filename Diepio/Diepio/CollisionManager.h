#pragma once
#ifndef __COLLISIONMGR__
#define __COLLISIONMGR__

class CGameObject;

class CCollisionManager
{
public:
	CCollisionManager();
	~CCollisionManager();


public:
	static bool CollisionbyObjects(CGameObject* obj, CGameObject* obj2);
};

#endif
