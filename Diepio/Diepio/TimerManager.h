#pragma once
#ifndef __TIMERMGR__
#define __TIMERMGR__
#include "Define.h"

class CTimerManager
{
	DECLARE_SINGLETON(CTimerManager);

private:
	explicit CTimerManager();
	virtual ~CTimerManager();

public:
	const float& GetDelta() const;
public:
	void UpdateTime();

private:
	LARGE_INTEGER m_CurTime;
	LARGE_INTEGER m_OldTime;
	LARGE_INTEGER m_CpuTick;
	float m_fDeltaTime;

};

#endif
