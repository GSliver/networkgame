#pragma once
#ifndef __ABSTRACT__
#define __ABSTRACT__

template<class T>
class CAbstractFactory {
public:
	static CGameObject* Create() {
		CGameObject* pObj = new T;
		pObj->Initialize();
		return pObj;
	}

	static CGameObject* Create(float x, float y) {
		CGameObject* pObj = new T;
		pObj->Initialize();
		pObj->Set_Position(x, y);
		return pObj;
	}
	static CGameObject* Create(OBJECTTYPE type, float x, float y)	{
		CGameObject* pObj = new T;
		pObj->Initialize();
		pObj->Set_Position(x, y);
		pObj->Set_ObjectId(type);
		return pObj;
	}
};

#endif // !__ABSTRACT__
