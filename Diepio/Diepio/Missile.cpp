#include "stdafx.h"
#include "Missile.h"
#include "SceneManager.h"
CMissile::CMissile()
{
}

CMissile::~CMissile()
{
}

void CMissile::Initialize()
{
	m_Info.fx = 0.f;
	m_Info.fy = 0.f;
	m_Info.fsizex = 20.f;
	m_Info.fsizey = 20.f;
}

int CMissile::Update()
{
	if (m_IsDead)
		return OBJ_DEAD;
	if (CSceneManager::GetInstance()->Get_SceneID() != SCENEID::SCENE_STAGE) //stage�� �ƴҽ� x
		return OBJ_DEAD;

	return OBJ_NOEVENT;
}

void CMissile::LateUpdate()
{
	if (m_IsDead)
		return;
	if (CSceneManager::GetInstance()->Get_SceneID() != SCENEID::SCENE_STAGE) //stage�� �ƴҽ� x
		return;
}

void CMissile::Render(HDC hDC)
{
	if (m_IsDead)
		return;
	if (CSceneManager::GetInstance()->Get_SceneID() != SCENEID::SCENE_STAGE) //stage�� �ƴҽ� x
		return;

	CGameObject::UpdateRect();

	HPEN hPen = CreatePen(PS_SOLID, 5, RGB(76, 75, 74));
	HPEN oldPen = (HPEN)SelectObject(hDC, hPen);

	HBRUSH hBrush = (HBRUSH)CreateSolidBrush(RGB(255, 102, 153));
	HBRUSH oldBrush = (HBRUSH)SelectObject(hDC, hBrush);

	Ellipse(hDC, m_Rect.left, m_Rect.top, m_Rect.right, m_Rect.bottom);

	SelectObject(hDC, oldPen);
	DeleteObject(hPen);
	SelectObject(hDC, oldBrush);
	DeleteObject(hBrush);
}

void CMissile::Release()
{
}
