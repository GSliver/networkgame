#pragma once
#ifndef __WAITROOM__
#define __WAITROOM__

#include "Scene.h"

class CWaitRoom : public CScene
{
public:
	explicit CWaitRoom();
	virtual ~CWaitRoom();

	// CScene을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hdc) override;
	virtual void Release() override;

public:

private:
	int playerNum = 0;

	//3초후 대기실 넘어가는 용도.
	float fTime = 0.f;
};

#endif // !__WAITROOM__


