#include "stdafx.h"
#include "BitmapManager.h"
#include "BitmapResource.h"


CBitmapManager* CBitmapManager::m_pInstance = nullptr;

CBitmapManager::CBitmapManager()
{
}


CBitmapManager::~CBitmapManager()
{
}


HDC CBitmapManager::FindImage(const TCHAR* pImgKey)
{
	const auto& iter = find_if(m_BmpMap.begin(), m_BmpMap.end(), [&](auto& rPair)
		{
			return !lstrcmp(pImgKey, rPair.first);
		});
	if (m_BmpMap.end() == iter) {
		cout << "그림이 없습니다." << endl;
		return nullptr;
	}

	return iter->second->Get_MemDC(); // 그림 있으면 그림 반환
}

void CBitmapManager::InsertBmp(const TCHAR* pFilePath, const TCHAR* pImgKey)
{
	const auto& iter = find_if(m_BmpMap.begin(), m_BmpMap.end(), [&](auto& rPair)
		{
			return !lstrcmp(pImgKey, rPair.first);
		});

	if (m_BmpMap.end() == iter) { // 키값과 일치하는 또다른 키값이 없다.
		//그림이 없으니 그림삽입
		CBitmapResource* TempBit = new CBitmapResource;
		TempBit->LoadBmp(pFilePath);	//파일 경로
		m_BmpMap.emplace(pImgKey, TempBit);
	}
	else {
		//그림이 이미 있음
		return;
	}
}

void CBitmapManager::Release()
{
	//first - 키값, second - Bitmap
	for (auto& iter : m_BmpMap) {
		if (iter.second) {  //비트맵 들어가있으면
			delete iter.second;
			iter.second = nullptr;
		}
	}
	m_BmpMap.clear();
}
