#include "stdafx.h"
#include "SceneManager.h"
#include "Obstacle.h"

CObstacle::CObstacle()
{
}

CObstacle::~CObstacle()
{
}

void CObstacle::Initialize()
{
	m_Info.fx = 0.f;
	m_Info.fy = 0.f;
	m_Info.fsizex = 100.f;
	m_Info.fsizey = 100.f;
}

int CObstacle::Update()
{
	return OBJ_NOEVENT;
}

void CObstacle::LateUpdate()
{
}

void CObstacle::Render(HDC hDC)
{
	if (m_IsDead)
		return;
	if (CSceneManager::GetInstance()->Get_SceneID() != SCENEID::SCENE_STAGE) //stage�� �ƴҽ� x
		return;

	CGameObject::UpdateRect();

	//��ֹ� ��		
	HPEN hPen = CreatePen(PS_SOLID, 5, RGB(76, 75, 74));
	HPEN oldPen = (HPEN)SelectObject(hDC, hPen);

	HBRUSH hBrush = (HBRUSH)CreateSolidBrush(RGB(255, 211, 77));
	HBRUSH oldBrush = (HBRUSH)SelectObject(hDC, hBrush);

	Ellipse(hDC, m_Rect.left, m_Rect.top, m_Rect.right, m_Rect.bottom);

	SelectObject(hDC, oldPen);
	DeleteObject(hPen);
	SelectObject(hDC, oldBrush);
	DeleteObject(hBrush);
}

void CObstacle::Release()
{
}
