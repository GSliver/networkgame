#pragma once
#ifndef __SCENEMGR__
#define __SCENEMGR__
#include "Define.h"

class CScene;

class CSceneManager
{
	DECLARE_SINGLETON(CSceneManager)

public:
	void Update();
	void LateUpdate();
	void Render(HDC hDC);
	void Release();

	void SceneChange(SCENEID eID);

	//get
	CScene* Get_Scene() { return m_pScene; }
	const SCENEID Get_SceneID() { return m_eCurScene; }
private:
	CSceneManager();
	~CSceneManager();
private:
	CScene* m_pScene;

	SCENEID m_eCurScene;
	SCENEID m_eNextScene;
};

#endif
