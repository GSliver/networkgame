#include "stdafx.h"
#include "Menu.h"
#include "BitmapManager.h"
#include "SceneManager.h"
#include "ObjectManager.h"
#include "AbstractFactory.h"
#include "GameManager.h"
#include "ClickButton.h"
#include "KeyMgr.h"
#include "Player.h"
CMenu::CMenu()
{
}

CMenu::~CMenu()
{
	Release();
}

void CMenu::Initialize()
{
	ShowCursor(true);
	//resource load
	CBitmapManager::GetInstance()->InsertBmp(L"../Resources/BackGround/Robby.bmp", L"Menu");
	CBitmapManager::GetInstance()->InsertBmp(L"../Resources/UI/StartButton.bmp", L"StartButton");
	CBitmapManager::GetInstance()->InsertBmp(L"../Resources/UI/ReadyState.bmp", L"ReadyState");

	CGameObject* temp = CAbstractFactory<CClickButton>::Create(650.f, 560.f);
	dynamic_cast<CClickButton*>(temp)->SetButtonType(BUTTONTYPE::LOBBY);
	temp->Set_Size(438.f, 154.f);
	CObjectManager::GetInstance()->AddObject(BUTTON, temp);
}

void CMenu::Update()
{
	CObjectManager::GetInstance()->Update();
}

void CMenu::LateUpdate()
{
	CObjectManager::GetInstance()->LateUpdate();

	if (dynamic_cast<CClickButton*>(CObjectManager::GetInstance()->GetObjectList(BUTTON).front())->Get_IsClicked())
	{

		//서버와 연결 
		CGameManager::GetInstance()->WsaStart();
		CGameManager::GetInstance()->CreateSocket();
		CGameManager::GetInstance()->ConnectToServer();
		CGameManager::GetInstance()->CreateRecvThread();

		CSceneManager::GetInstance()->SceneChange(SCENE_WAITROOM);
	}
}

void CMenu::Render(HDC hDC)
{
	HDC hBack = CBitmapManager::GetInstance()->FindImage(L"BackBuffer");//더블버퍼
	HDC hMemDC = CBitmapManager::GetInstance()->FindImage(L"Menu");//로고이미지
	BitBlt(hBack, 0, 0, 1280, 720, hMemDC, 0, 0, SRCCOPY);

	CObjectManager::GetInstance()->Render(hBack);
	
	BitBlt(hDC, 0, 0, 1280, 720, hBack, 0, 0, SRCCOPY);

	//입력창 그리기
	//if (nickname.length() > 0) {
	//	RECT rc = { 465, 335, 800, 355 };
	//	HFONT hFont = CreateFont(20, 0, 0, 0, FW_BOLD, 0, 0, 0, HANGEUL_CHARSET, 0, 0, 0,
	//		VARIABLE_PITCH | FF_ROMAN, TEXT("궁서"));
	//	HFONT oldFont = (HFONT)SelectObject(hDC, hFont);
	//	DrawText(hDC, nickname.c_str(), -1, &rc, DT_WORDBREAK);
	//	SelectObject(hDC, oldFont);
	//	DeleteObject(hFont);
	//}
}

void CMenu::Release()
{
	CObjectManager::GetInstance()->Delete_ID(BUTTON);
}

void CMenu::Input_nickname(WPARAM wParam)
{
	//if (wParam == VK_BACK) //back space
	//{
	//	if (nickname.length() > 0)
	//		nickname.erase(nickname.length() - 1);
	//}
	//
	//else
	//{
	//	if (nickname.length() < 30) {
	//		if (wParam >= 'A' && wParam <= 'z')
	//		{
	//			nickname += wParam;
	//		}
	//	}
	//}

}