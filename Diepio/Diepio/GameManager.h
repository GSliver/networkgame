#pragma once
#ifndef __GAMEMNG__
#define __GAMEMNG__
#define _CRT_SECURE_NO_WARNINGS         // 최신 VC++ 컴파일 시 경고 방지
#define _WINSOCK_DEPRECATED_NO_WARNINGS // 최신 VC++ 컴파일 시 경고 방지
#pragma comment(lib,"ws2_32")
#include<WinSock2.h>
#include<iostream>
#include "Define.h"

#define SERVERPORT 9000

class GameObject;
class CPlayer;
class CGameManager
{
	DECLARE_SINGLETON(CGameManager)

private:
	CGameManager();
	~CGameManager();

public:
	//server
	static void err_quit(const WCHAR* msg);
	static void err_display(const WCHAR* msg);

	//start
	int WsaStart(); //WSAStartup
	void CreateSocket(); //socket
	void ConnectToServer(); //connect

	//thread
	void CreateRecvThread(); //recv Thread create

	//ip
	void SetServerIP();

	//send recv
	
	void SendData(char dataindex, LPVOID packet);
	static void RecvInfo(); //in thread func
	static int recvn(SOCKET s, char* buf, int len, int flags); //in thread func

	//end
	void DisConnectServer();

public:
	//set
	void SetPlayer(CPlayer* pPlayer) { m_pPlayer = pPlayer; }
	void SetServerIndex(int index) { serverindex = index; }
	void InitializeNickName();
	void InitializeLivePlayerNum() { liveplayernum = 3; }
	int SubtractLivePlayerNum() { return --liveplayernum; }
	//get
	const CPlayer* GetPlayer() { return m_pPlayer; }
	const int GetPlayerServerID() { return serverindex; }
	void GetNickName(size_t index, wstring& nickname);
	const int& GetLivePlayerNum() { return liveplayernum; }
	const int& GetWinnderIndex() { return winnerplayerindex; }
	const char& GetServerPlayerNum() { return serverplayernum; }
	//
	void FindWinner();

	//server
	void AddMissileFromServer(BulletInfo bullet);

	static DWORD __stdcall RecvFromServer(void*);
private:
	CPlayer* m_pPlayer;
	static char serverindex;

	//server
	static int retval;

	WSADATA wsa;
	static SOCKET sock;
	SOCKADDR_IN serveraddr;
	char serverip[15];

	//server player
	vector<PlayerInfo> vPlayer; //서버에서 받아온 플레이어 정보
	vector<BulletInfo> vBullets; //서버에서 받아온 총알 정보
	static int indexset;
	static string waitroomNickname[3];
	static char serverplayernum;
	int liveplayernum = 0;
	int winnerplayerindex = -1;

	//bullet 
	static char bulletindex;
};

#endif // !__GAMEMNG__
