#include "stdafx.h"
#include "MainProg.h"
#include "TimerManager.h"
#include "SceneManager.h"
#include "BitmapManager.h"
#include "ObjectManager.h"
#include "AbstractFactory.h"
#include "KeyMgr.h"
#include "Mouse.h"
CMainProg::CMainProg()
	:m_TimeMgr(CTimerManager::GetInstance())
{
}

CMainProg::~CMainProg()
{
	Release();
}

void CMainProg::Initialize()
{
	m_hDC = GetDC(g_hWnd); 
	CKeyManager::GetInstance();
	CBitmapManager::GetInstance()->InsertBmp(L"../Resources/BackGround/BackBuffer.bmp",L"BackBuffer"); //백버퍼
	CObjectManager::GetInstance()->AddObject(MOUSE, CAbstractFactory<CMouse>::Create(400.f, 300.f)); // 마우스

	CSceneManager::GetInstance()->SceneChange(SCENEID::SCENE_LOGO); //로고로 씬 전환
}

void CMainProg::Update()
{
	m_TimeMgr->UpdateTime(); //타이머 업데이트

	CSceneManager::GetInstance()->Update();
	CKeyManager::GetInstance()->KeyUpdate();

}

void CMainProg::LateUpdate()
{
	CSceneManager::GetInstance()->LateUpdate();
}

void CMainProg::Render()
{
	CSceneManager::GetInstance()->Render(m_hDC);

	//FPS 출력
	m_fTimeCount += m_TimeMgr->GetDelta();
	++m_iFps;
	if (1.f <= m_fTimeCount)
	{
		swprintf_s(m_szFps, L"Diepo_FPS: %d", m_iFps);
		m_iFps = 0;
		m_fTimeCount = 0.f;
	}
	SetWindowText(g_hWnd, m_szFps);
}

void CMainProg::Release()
{
	CKeyManager::GetInstance()->DestroyInstance();
	CBitmapManager::GetInstance()->DestroyInstance();
	CSceneManager::GetInstance()->DestroyInstance();
	m_TimeMgr->DestroyInstance();

	ReleaseDC(g_hWnd, m_hDC);
}
