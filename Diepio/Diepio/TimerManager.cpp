#include "stdafx.h"
#include "TimerManager.h"

IMPLEMENT_SINGLETON(CTimerManager)

CTimerManager::CTimerManager()
	: m_fDeltaTime(0.f)
{
	QueryPerformanceCounter(&m_CurTime);
	QueryPerformanceCounter(&m_OldTime);

	QueryPerformanceFrequency(&m_CpuTick);
}


CTimerManager::~CTimerManager()
{
}

const float& CTimerManager::GetDelta() const
{
	// TODO: 여기에 반환 구문을 삽입합니다.
	return m_fDeltaTime;
}

void CTimerManager::UpdateTime()
{
	QueryPerformanceCounter(&m_CurTime);

	if (m_CurTime.QuadPart - m_OldTime.QuadPart > m_CpuTick.QuadPart)
	{
		QueryPerformanceCounter(&m_CurTime);
		QueryPerformanceCounter(&m_OldTime);
		QueryPerformanceFrequency(&m_CpuTick);
	}

	m_fDeltaTime = float(m_CurTime.QuadPart - m_OldTime.QuadPart) / m_CpuTick.QuadPart;
	m_OldTime.QuadPart = m_CurTime.QuadPart;
}
