#include "stdafx.h"
#include "WaitRoom.h"
#include "BitmapManager.h"
#include "ObjectManager.h"
#include "TimerManager.h"
#include "SceneManager.h"
#include "ObjectManager.h"
#include "GameManager.h"
#include "AbstractFactory.h"
#include "ClickButton.h"
#include "Player.h"
CWaitRoom::CWaitRoom()
{
}

CWaitRoom::~CWaitRoom()
{
	Release();
}

void CWaitRoom::Initialize()
{
	ShowCursor(true);
	CBitmapManager::GetInstance()->InsertBmp(L"../Resources/BackGround/Waitingroom_Start.bmp", L"Waitingroom_Start");
	CBitmapManager::GetInstance()->InsertBmp(L"../Resources/BackGround/Waitingroom_Wait.bmp", L"Waitingroom_Wait");
	CGameManager::GetInstance()->InitializeNickName();
}

void CWaitRoom::Update()
{
	//서버로 부터 플레이어 리스트 받아옴
	playerNum = CObjectManager::GetInstance()->GetObjectList(OBJECTTYPE::PLAYER).size(); //현재 플레이어 수

	CObjectManager::GetInstance()->Update();
}

void CWaitRoom::LateUpdate()
{
	CObjectManager::GetInstance()->LateUpdate();

	//준비된 플레이어가 3명이면 3초후 Stage로 씬 전환
	//수정
	if (playerNum == 3)
	{
		fTime += CTimerManager::GetInstance()->GetDelta();

		if (fTime > 3.f)
		{
			fTime = 0.f;
			CSceneManager::GetInstance()->SceneChange(SCENEID::SCENE_STAGE);
		}
	}

}

void CWaitRoom::Render(HDC hdc)
{
	HDC hBack = CBitmapManager::GetInstance()->FindImage(L"BackBuffer");//더블버퍼
	HDC hMemDC = 0;
	if (playerNum < 3)
		hMemDC = CBitmapManager::GetInstance()->FindImage(L"Waitingroom_Wait");//로고이미지
	else
		hMemDC = CBitmapManager::GetInstance()->FindImage(L"Waitingroom_Start");

	BitBlt(hBack, 0, 0, 1280, 720, hMemDC, 0, 0, SRCCOPY);

	CObjectManager::GetInstance()->Render(hBack);

	BitBlt(hdc, 0, 0, 1280, 720, hBack, 0, 0, SRCCOPY);

	//NickName출력
	for (size_t i = 0; i < CGameManager::GetInstance()->GetServerPlayerNum(); ++i)
	{
		wstring nickname;
		CGameManager::GetInstance()->GetNickName(i, nickname);
		RECT rc = { 135+(400*i), 485, 365+(400*i), 505 };
		
		//font
		HFONT hFont = CreateFont(20, 0, 0, 0, FW_BOLD, 0, 0, 0, HANGEUL_CHARSET, 0, 0, 0,
			VARIABLE_PITCH | FF_ROMAN, TEXT("궁서"));
		HFONT oldFont = (HFONT)SelectObject(hdc, hFont);
		DrawText(hdc, nickname.c_str(), -1, &rc, DT_WORDBREAK|DT_CENTER);
		SelectObject(hdc, oldFont);
		DeleteObject(hFont);
	}
}

void CWaitRoom::Release()
{
	CObjectManager::GetInstance()->Delete_ID(BUTTON);
}
