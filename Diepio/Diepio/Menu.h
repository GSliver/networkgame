#pragma once
#ifndef __MENU__
#define __MENU__

#include "Scene.h"

class CMenu: public CScene
{
public:
	explicit CMenu();
	virtual ~CMenu();

	// CScene을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;

public:
	void Input_nickname(WPARAM wParam);

private:
	wstring nickname;

};

#endif // !__MENU__
