#include "stdafx.h"
#include "Player.h"
#include "KeyMgr.h"
#include "GameManager.h"
#include "TimerManager.h"
#include "SceneManager.h"
#include "ObjectManager.h"

CPlayer::CPlayer()
{
	Initialize();
}

CPlayer::~CPlayer()
{
	Release();
}

void CPlayer::Initialize()
{
	m_Info.fx = 0.f;
	m_Info.fy = 0.f;
	m_Info.fsizex = 50.f;
	m_Info.fsizey = 50.f;
	m_fMaxHP = 100.f;
	mouse.isClicked = false;

}

int CPlayer::Update()
{
	if (m_IsDead) //플레이어는 게임 도중에 삭제 x
		return OBJ_NOEVENT;
	if (CSceneManager::GetInstance()->Get_SceneID() != SCENEID::SCENE_STAGE) //stage씬 아닐시 x
		return OBJ_NOEVENT;

	if (m_fHp <= 0.f)
	{
		m_IsDead = true;
		if (CGameManager::GetInstance()->SubtractLivePlayerNum() == 1) //플레이어 한명 빼기
		{
			CGameManager::GetInstance()->FindWinner();
		}
	}
	KeyInput();

	return OBJ_NOEVENT;
}

void CPlayer::LateUpdate()
{
	if (m_IsDead) 
		return;
	if (CSceneManager::GetInstance()->Get_SceneID() != SCENEID::SCENE_STAGE) //stage씬 아닐시 x
		return;
}

void CPlayer::Render(HDC hDC)
{
	if (m_IsDead)
		return;
	if (CSceneManager::GetInstance()->Get_SceneID() != SCENEID::SCENE_STAGE) //stage씬 아닐시 x
		return;

	CGameObject::UpdateRect();

	//Player Hp

	//MaxHP
	hPen = CreatePen(PS_SOLID, 0, RGB(0, 0, 0));
	oldPen = (HPEN)SelectObject(hDC, hPen);

	hBrush = (HBRUSH)CreateSolidBrush(RGB(255, 255, 255));
	oldBrush = (HBRUSH)SelectObject(hDC, hBrush);

	Rectangle(hDC, m_Rect.left, m_Rect.top - 40, m_Rect.right, m_Rect.top - 30);

	SelectObject(hDC, oldPen);
	DeleteObject(hPen);

	SelectObject(hDC, oldBrush);
	DeleteObject(hBrush);

	//CurrentHP

	if (m_fHp > 50.f)
		hBrush = (HBRUSH)CreateSolidBrush(RGB(200 - ((int)m_fHp * 2), 200, 0));
	else
		hBrush = (HBRUSH)CreateSolidBrush(RGB(200 , (int)m_fHp * 2, 0));

	oldBrush = (HBRUSH)SelectObject(hDC, hBrush);

	Rectangle(hDC, m_Rect.left, m_Rect.top - 40, m_Rect.left + (m_Rect.right - m_Rect.left) * (m_fHp/ m_fMaxHP), m_Rect.top - 30);


	SelectObject(hDC, oldBrush);
	DeleteObject(hBrush);

	//Gun Color
	hPen = CreatePen(PS_SOLID, 20, RGB(153, 153, 153));
	oldPen = (HPEN)SelectObject(hDC, hPen);

	MoveToEx(hDC, gunx1, guny1, NULL);
	LineTo(hDC, gunx2, guny2);

	SelectObject(hDC, oldPen);
	DeleteObject(hPen);

	//Body Color
	switch (m_serverindex)
	{
	case 0://red
	{
		hPen = CreatePen(PS_SOLID, 5, RGB(76, 75, 74));
		oldPen = (HPEN)SelectObject(hDC, hPen);

		hBrush = (HBRUSH)CreateSolidBrush(RGB(233, 65, 6));
		oldBrush = (HBRUSH)SelectObject(hDC, hBrush);
	}
		break;
	case 1://green
	{
		hPen = CreatePen(PS_SOLID, 5, RGB(76, 75, 74));
		oldPen = (HPEN)SelectObject(hDC, hPen);

		hBrush = (HBRUSH)CreateSolidBrush(RGB(112, 173, 71));
		oldBrush = (HBRUSH)SelectObject(hDC, hBrush);
	}
		break;
	case 2:
	{
		hPen = CreatePen(PS_SOLID, 5, RGB(76, 75, 74));
		oldPen = (HPEN)SelectObject(hDC, hPen);

		hBrush = (HBRUSH)CreateSolidBrush(RGB(6, 174, 233));
		oldBrush = (HBRUSH)SelectObject(hDC, hBrush);
	}
		break;
	default:
		break;
	}

	//Body
	Ellipse(hDC, m_Rect.left, m_Rect.top, m_Rect.right, m_Rect.bottom);

	SelectObject(hDC, oldPen);
	DeleteObject(hPen);
	SelectObject(hDC, oldBrush);
	DeleteObject(hBrush);

}

void CPlayer::Release()
{
}

void CPlayer::KeyInput()
{
	if (m_serverindex == CGameManager::GetInstance()->GetPlayerServerID())
	{
		KeyInfo key;
		ZeroMemory(&key, sizeof(KeyInfo));
		key.pIndex = m_serverindex;

		if (CKeyManager::GetInstance()->KeyPressing('w') ||
			CKeyManager::GetInstance()->KeyPressing('W'))
		{
			//위 키 누른 정보 전달
			key.w = true;
		}

		if (CKeyManager::GetInstance()->KeyPressing('a') ||
			CKeyManager::GetInstance()->KeyPressing('A'))
		{
			//왼쪽 키 누른 정보 전달
			key.a = true;
		}

		if (CKeyManager::GetInstance()->KeyPressing('d') ||
			CKeyManager::GetInstance()->KeyPressing('D'))
		{
			//오른쪽 키 누른 정보 전달
			key.d = true;
		}

		if (CKeyManager::GetInstance()->KeyPressing('s') ||
			CKeyManager::GetInstance()->KeyPressing('S'))
		{
			//아래 키 누른 정보 전달
			key.s = true;
		}
	
		if (key.w || key.a || key.d || key.s) {
			CGameManager::GetInstance()->SendData(KEYINFO, &key);
		}

		//마우스 좌표 전달
		MouseupdateDelta += CTimerManager::GetInstance()->GetDelta();
		if (MouseupdateDelta > 0.04f)
		{
			//마우스 좌표 
			MouseupdateDelta = 0.f;
			INFO mousepos = *(CObjectManager::GetInstance()->GetObjectList(MOUSE).front())->Get_Info();
			mouse.mx = mousepos.fx;
			mouse.my = mousepos.fy;
			CGameManager::GetInstance()->SendData(MOUSEINFO, &mouse);
			mouse.isClicked = false;
		}

		//마우스 클릭 시 총알 생성 true 및 위치 전달
		if (CKeyManager::GetInstance()->KeyDown(VK_LBUTTON))
		{
			if (CObjectManager::GetInstance()->GetObjectList(MISSILE).size() < 24)
				mouse.isClicked = true;
		}
	}
}
