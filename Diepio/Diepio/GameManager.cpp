#include "stdafx.h"
#include "GameManager.h"
#include "Player.h"
#include "Obstacle.h"
#include "Missile.h"
#include "AbstractFactory.h"
#include "ObjectManager.h"
#include<string>
IMPLEMENT_SINGLETON(CGameManager)
int CGameManager::retval;
SOCKET CGameManager::sock;
char CGameManager::serverindex = -1;
int CGameManager::indexset = 0;
char CGameManager::bulletindex = 0;
string CGameManager::waitroomNickname[3];
char CGameManager::serverplayernum = 0;
CGameManager::CGameManager()
{

}

CGameManager::~CGameManager()
{
	//뮤텍스 닫기
	CloseHandle(Bulletmutex);

	//소켓 닫기
	closesocket(sock);

	//윈속 종료
	WSACleanup();
}

void CGameManager::err_quit(const WCHAR* msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, WSAGetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL);
	MessageBox(NULL, (LPCTSTR)lpMsgBuf, msg, MB_ICONERROR);
	LocalFree(lpMsgBuf);
	exit(1);
}

void CGameManager::err_display(const WCHAR* msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, WSAGetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL);
	//cout << "[" << msg << "]" << (char*)lpMsgBuf << endl;
	LocalFree(lpMsgBuf);
}

int CGameManager::WsaStart()
{
	if (WSAStartup(MAKEWORD(1, 1), &wsa) != 0)
		return 1;
}

void CGameManager::CreateSocket()
{
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == INVALID_SOCKET)
		err_quit(L"socket()");
}

void CGameManager::ConnectToServer()
{
	SetServerIP();

	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = inet_addr(serverip);
	serveraddr.sin_port = htons(SERVERPORT);
	retval = connect(sock, (SOCKADDR*)&serveraddr, sizeof(serveraddr));
	if (retval == SOCKET_ERROR)
		err_quit(L"connect()");

	Bulletmutex = CreateMutex(NULL, FALSE, NULL);
}

void CGameManager::CreateRecvThread()
{
	HANDLE RecvThread = CreateThread(NULL, 0, this->RecvFromServer , NULL, 0, NULL);
	if (NULL == RecvThread)
		cout << "recvThread Create Err" << endl;
}

void CGameManager::SetServerIP()
{
	cout << "SERVERIP: ";
	cin >> serverip;
}

void CGameManager::SendData(char datainfo, LPVOID packet)
{
	DataInfo d;
	d.infoindex = datainfo;
	switch (d.infoindex)
	{
	case KEYINFO: //KEYINFO
	{
		d.datasize = sizeof(KeyInfo);
		KeyInfo info = *(KeyInfo*)packet;
		//고정 길이
		retval = send(sock, (char*)&d, sizeof(DataInfo), 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"KeyInfo 고정길이 send() err");
			return;
		}
		//가변 길이
		retval = send(sock, (char*)&info, d.datasize, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"KeyInfo 가변길이 send() err");
			return;
		}
	}
		break;
	case MOUSEINFO: //MOUSEINFO
	{
		d.datasize = sizeof(MouseInfo);
		MouseInfo info = *(MouseInfo*)packet;
		//고정 길이
		retval = send(sock, (char*)&d, sizeof(DataInfo), 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"MouseInfo 고정길이 send() err");
			return;
		}
		//가변 길이
		retval = send(sock, (char*)&info, d.datasize, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"MouseInfo 가변길이 send() err");
			return;
		}
	}
	break;
	case PLAYERINFO: //PLAYERINFO
	{
		d.datasize = sizeof(PlayerInfo);
	}
		break;
	case BULLETCREATE: //BULLETCREATE
	{
	}
		break;
	case BULLETINFO: //BULLETINFO
	{
		d.datasize = sizeof(BulletInfo);
		BulletInfo info = *(BulletInfo*)packet;
		//고정 길이
		retval = send(sock, (char*)&d, sizeof(DataInfo), 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"BulletInfo 고정길이 send() err");
			return;
		}
		//가변 길이
		retval = send(sock, (char*)&info, d.datasize, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"BulletInfo 가변길이 send() err");
			return;
		}
	}
		break;
	case OBSTACLEINFO: //OBSTACLEINFO
	{

	}
		break;
	case GAMESTATE:
	{

	}
		break;
	case PLAYERINDEX:
	{

	}
		break;
	default:
		break;
	}
}

void CGameManager::RecvInfo()
{
	DataInfo d;

	//고정길이
	retval = recvn(sock, (char*)&d, sizeof(DataInfo), 0);
	//가변길이
	switch (d.infoindex)
	{
	case KEYINFO: //KEYINFO
		break;
	case MOUSEINFO: //MOUSEINFO
		break;
	case PLAYERINFO: //PLAYERINFO
	{
		PlayerInfo Info;
		retval = recvn(sock, (char*)&Info, sizeof(PlayerInfo), 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"PlayerInfo recv err");
		}
		
		if (indexset < 3)
		{
			CGameObject* pPlayer = CAbstractFactory<CPlayer>::Create(Info.x, Info.y);
			dynamic_cast<CPlayer*>(pPlayer)->SetServerIndex(indexset++);
			CObjectManager::GetInstance()->AddObject(PLAYER, pPlayer);
		}
		CObjectManager::GetInstance()->PlayerUpdate(Info);
	}
		break;
	case BULLETCREATE:
	{
		BulletInfo Info;
		retval = recvn(sock, (char*)&Info, sizeof(BulletInfo), 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"BulletInfo recv err");
		}
		CGameObject* pMissile = CAbstractFactory<CMissile>::Create(Info.x, Info.y);
		dynamic_cast<CMissile*>(pMissile)->Set_Direction(Info.dx, Info.dy);
		dynamic_cast<CMissile*>(pMissile)->Set_PlayerId(Info.pIndex);
		dynamic_cast<CMissile*>(pMissile)->Set_index(bulletindex++);
		CObjectManager::GetInstance()->AddObject(MISSILE, pMissile);
		
	}
		break;
	case BULLETINFO: //BULLETINFO
	{
		BulletInfo Info;
		retval = recvn(sock, (char*)&Info, sizeof(BulletInfo), 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"BulletInfo recv err");
		}
		CObjectManager::GetInstance()->BulletUpdate(Info);
	}
		break;
	case OBSTACLEINFO: //OBSTACLEINFO
	{
		ObstacleInfo Info;
		retval = recvn(sock, (char*)&Info, sizeof(ObstacleInfo), 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"ObstacleInfo recv err");
		}
		CGameObject* pObstacle = CAbstractFactory<CObstacle>::Create(Info.x, Info.y);
		CObjectManager::GetInstance()->AddObject(OBSTACLE, pObstacle);
	}
		break;
	case GAMESTATE: //GAMESTATE
	{

	}
	break;
	case PLAYERINDEX: //PLAYERINDEX
	{
		//서버로 부터의 플레이어 인덱스
		char Info;
		retval = recvn(sock, (char*)&Info, sizeof(char), 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"playerindex recv err");
		}
		if (serverindex < 0)
			serverindex = Info;
	}
		break;
	case PLAYERACCEPT:
	{
		//접속한 플레이어 수
		char Info;
		retval = recvn(sock, (char*)&Info, sizeof(char), 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"playerindex recv err");
		}
		serverplayernum = Info;
	}
		break;
	default:
		break;
	}
	
}

int CGameManager::recvn(SOCKET s, char* buf, int len, int flags)
{
	int received;
	char* ptr = buf;
	int left = len;

	while (left > 0) {
		received = recv(s, ptr, left, flags);
		if (received == SOCKET_ERROR)
			return SOCKET_ERROR;
		else if (received == 0)
			break;
		left -= received;
		ptr += received;
	}
	return (len - left);
}

void CGameManager::DisConnectServer()
{
	closesocket(sock);
	WSACleanup();
}


void CGameManager::InitializeNickName()
{
	for (int i = 0; i < 3; ++i)
		waitroomNickname[i] = "Player" + to_string(i+1);
}

void CGameManager::GetNickName(size_t index, wstring& nickname)
{
	// TODO: 여기에 return 문을 삽입합니다.
	nickname.assign(waitroomNickname[index].begin(), waitroomNickname[index].end());
}

void CGameManager::FindWinner()
{
	winnerplayerindex = CObjectManager::GetInstance()->GetWinnerIndex();
}

void CGameManager::AddMissileFromServer(BulletInfo bullet)
{
	CGameObject* pMissile = new CMissile;
	dynamic_cast<CMissile*>(pMissile)->Set_Position(bullet.x, bullet.y);
	CObjectManager::GetInstance()->AddObject(OBJECTTYPE::MISSILE, pMissile);
}

DWORD __stdcall CGameManager::RecvFromServer(void*)
{
	while (true) {
		RecvInfo();
	}
	return 0;
}
