#pragma once

#pragma comment(lib, "ws2_32")
#include <WinSock2.h>
#include <profileapi.h>
#include <math.h>
#include <list>
#include <iostream>
#include "Error.h"
#include "define.h"
using namespace std;

typedef struct ClientInfo {
	SOCKET clientsock;
};

typedef struct DataInfo {
	char infoindex;
	char datasize;
};

//Ű�Է�
typedef struct KeyInfo {
	char pIndex;      //�÷��̾ �����ϱ� ���� �ε���
	bool w;            //w �̵�
	bool a;            //a �̵�
	bool s;            //s �̵�
	bool d;            //d �̵�
};

//���콺
typedef struct MouseInfo {
	char pIndex;      //�÷��̾ �����ϱ� ���� �ε���
	float mx;         //���콺 x��ǥ
	float my;         //���콺 y��ǥ
	bool isClicked;      //���콺 Ŭ�� ����(��ź �߻�)
};

typedef struct PlayerInfo {
	char pIndex;      //�÷��̾ �����ϱ� ���� �ε���
	float x;         //�÷��̾��� X��ǥ
	float y;         //�÷��̾��� Y��ǥ
	char nowHp;         //�÷��̾��� ���� ü��
	char maxHp;         //�÷��̾��� �ִ� ü��
	float gunx1;      //�ѱ��� �� X��ǥ 1
	float guny1;      //�ѱ��� �� Y��ǥ 1
	float gunx2;      //�ѱ��� �� X��ǥ 2
	float guny2;      //�ѱ��� �� Y��ǥ 2
};

//��ź ���� ����ü
typedef struct BulletInfo {
	char pIndex;      //�÷��̾ �����ϱ� ���� �ε���
	char bindex;      //��ź�� �����ϱ� ���� �ε���
	bool isDead;
	float x;         //��ź�� X��ǥ
	float y;         //��ź�� Y��ǥ
	float dx;       //��ź�� x���� ����
	float dy;       //��ź�� y���� ����
};

typedef struct ObstacleInfo {
	float x;         //��ֹ��� X��ǥ
	float y;         //��ֹ��� Y��ǥ
};

typedef struct Vector2D {
	float x;
	float y;
};

class CServer {
public:

public:
	int InitServer();
	void RunServer();

	static DWORD __stdcall UpdateThread(LPVOID arg);

	void InitObjects();
	static void Send_PlayerInfo();
	static void Send_ObstacleInfo();
	static void Send_CreateBullet(BulletInfo bInfo);
	static void Send_BulletInfo(BulletInfo bInfo);
	static void Recv_Info(LPVOID arg);
	static int recvn(SOCKET s, char* buf, int len, int flags);
	static void UpdatePlayerPos(KeyInfo kInfo);
	static void UpdateMuzzleInfo(MouseInfo mInfo);
	static void CreateBullet(char pIndex);
	static void UpdateBulletInfo(BulletInfo bInfo);
	static void CollisionBullet(BulletInfo bInfo);
	static void CollisionPlayerObstacle(char cIndex);

private:
	static CError* error;
	SOCKET listen_sock;
};