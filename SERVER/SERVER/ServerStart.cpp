#include "ServerMain.h"
#include "FrameManager.h"
#include "TimerManager.h"
CServer Server;

CError* CServer::error;
DWORD __stdcall FrameThread(LPVOID arg);

int main()
{
    //서버 초기화
    Server.InitServer();
    HANDLE fthread = CreateThread(NULL, 0, FrameThread, NULL, 0, NULL);
    Server.RunServer();
}

DWORD __stdcall FrameThread(LPVOID arg)
{
    //Update frame, timer
    while (1)
    {
        if (CFrameManager::GetInstance()->FrameLimit(40.f))
        {
            CTimerManager::GetInstance()->UpdateTime();
        }
    }
}