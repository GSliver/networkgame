#pragma once

#pragma comment(lib, "ws2_32")
#include <winsock2.h>
#include <stdio.h>

class CError
{
public:
    void err_quit(const char* msg);
    void err_display(const char* msg);
};