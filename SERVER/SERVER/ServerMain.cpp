#define _WINSOCK_DEPRECATED_NO_WARNINGS // 최신 VC++ 컴파일 시 경고 방지
#include <string>
#include <vector>
#include "ServerMain.h"
#include "TimerManager.h"
#include "FrameManager.h"

ClientInfo clientdata[3];
PlayerInfo playerdata[3];
ObstacleInfo obstacledata[3];

CRITICAL_SECTION cs;

int CServer::InitServer() {
    InitializeCriticalSection(&cs);
    int retval;

    // 윈속 초기화
    WSADATA wsa;
    if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
        return 1;

    // socket()
    listen_sock = socket(AF_INET, SOCK_STREAM, 0);
    if (listen_sock == INVALID_SOCKET) error->err_quit("socket()");

    // bind()
    SOCKADDR_IN serveraddr;
    ZeroMemory(&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serveraddr.sin_port = htons(SERVERPORT);
    retval = bind(listen_sock, (SOCKADDR*)&serveraddr, sizeof(serveraddr));
    if (retval == SOCKET_ERROR) error->err_quit("bind()");

    // listen()
    retval = listen(listen_sock, SOMAXCONN);
    if (retval == SOCKET_ERROR) error->err_quit("listen()");

    return 1;
}

void CServer::RunServer() {
    int retval;

    // 전용 소켓
    SOCKET client_sock;
    SOCKADDR_IN clientaddr;
    int addrlen;
    char cIndex = 0;

    while (1) {
        // accept()
        addrlen = sizeof(clientaddr);
        client_sock = accept(listen_sock, (SOCKADDR*)&clientaddr, &addrlen);
        if (client_sock == INVALID_SOCKET) {
            error->err_display((char*)"accept()");
            break;
        }

        // 자기 인덱스 전송
        DataInfo data;
        data.infoindex = PLAYERINDEX;
        data.datasize = sizeof(char);

        // 고정 길이
        retval = send(client_sock, (char*)&data, sizeof(DataInfo), 0);
        if (retval == SOCKET_ERROR) {
            error->err_display((char*)"CServer::RunServer() -> 인덱스 전송(고정 길이)");
            return;
        }

        // 가변 길이
        retval = send(client_sock, (char*)&cIndex, data.datasize, 0);
        if (retval == SOCKET_ERROR) {
            error->err_display((char*)"CServer::RunServer() -> 인덱스 전송(가변 길이)");
            return;
        }

        // 접속한 클라이언트 정보 출력
        printf("[TCP 서버] 클라이언트 접속 : IP 주소 = %s, 포트 번호 = %d, 인덱스 = %d\n", inet_ntoa(clientaddr.sin_addr), ntohs(clientaddr.sin_port), cIndex);

        // 스레드 생성
        HANDLE rthread = CreateThread(NULL, 0, UpdateThread, (LPVOID)client_sock, 0, NULL);
        clientdata[cIndex].clientsock = client_sock;
        playerdata[cIndex].pIndex = cIndex;
        cIndex++;

        // 모든 인덱스 전송
        for (int i = 0; i < cIndex; ++i)
        {
            DataInfo data;
            data.infoindex = PLAYERACCEPT;
            data.datasize = sizeof(char);

            // 고정 길이
            retval = send(clientdata[i].clientsock, (char*)&data, sizeof(DataInfo), 0);
            if (retval == SOCKET_ERROR) {
                error->err_display((char*)"CServer::RunServer() -> 인덱스 전송(고정 길이)");
                return;
            }

            // 가변 길이
            retval = send(clientdata[i].clientsock, (char*)&cIndex, data.datasize, 0);
            if (retval == SOCKET_ERROR) {
                error->err_display((char*)"CServer::RunServer() -> 인덱스 전송(가변 길이)");
                return;
            }
        }

        if (cIndex == MAX_USER) //클라이언트가 3명 접속하면
        {
            InitObjects();
        }
    }
}

DWORD __stdcall CServer::UpdateThread(LPVOID arg)
{
    while (1)
    {
        Recv_Info(arg);
    }
    return 0;
}

void CServer::InitObjects() //플레이어, 장애물 초기 좌표 설정
{
    memset(&playerdata, 0, sizeof(PlayerInfo));
    memset(&obstacledata, 0, sizeof(ObstacleInfo));

    //플레이어 인덱스
    for (int i = 0; i < 3; i++)
    {
        playerdata[i].pIndex = i;
    }

    //플레이어의 초기 좌표를 계산
    playerdata[0].x = 320.f; playerdata[0].y = 180.f;
    playerdata[1].x = 960.f; playerdata[1].y = 360.f;
    playerdata[2].x = 640.f; playerdata[2].y = 540.f;

    //장애물의 초기 좌표를 계산
    obstacledata[0].x = 320.f; obstacledata[0].y = 450.f;
    obstacledata[1].x = 800.f; obstacledata[1].y = 270.f;
    obstacledata[2].x = 960.f; obstacledata[2].y = 540.f;

    //총구의 초기 좌표를 계산, 체력 설정
    for (int i = 0; i < 3; i++)
    {
        playerdata[i].gunx1 = playerdata[i].x;
        playerdata[i].guny1 = playerdata[i].y;
        playerdata[i].gunx2 = playerdata[i].x;
        playerdata[i].guny2 = playerdata[i].y + 50.f;
        playerdata[i].nowHp = 100.f;
        playerdata[i].maxHp = 100.f;
    }
    Send_PlayerInfo();
    Send_ObstacleInfo();
}

void CServer::Send_PlayerInfo() //모든 클라이언트에 플레이어 정보 send
{
    int retval;

    DataInfo data;
    data.infoindex = PLAYERINFO;
    data.datasize = sizeof(PlayerInfo);

    for (int i = 0; i < 3; i++)
    {
        SOCKET client_sock = clientdata[i].clientsock;
        for (int j = 0; j < 3; j++)
        {
            // PlayerInfo 전송 (고정 길이)
            retval = send(client_sock, (char*)&data, sizeof(DataInfo), 0);
            if (retval == SOCKET_ERROR) {
                error->err_display((char*)"Send_PlayerInfo -> PlayerInfo 전송(고정 길이)");
                return;
            }

            // PlayerInfo 전송 (가변 길이)
            retval = send(client_sock, (char*)&playerdata[j], data.datasize, 0);
            if (retval == SOCKET_ERROR) {
                error->err_display((char*)"Send_PlayerInfo -> PlayerInfo 전송(가변 길이)");
                return;
            }
        }
    }
}

void CServer::Send_ObstacleInfo() //모든 클라이언트에 장애물 정보 send
{
    int retval;

    DataInfo data;
    data.infoindex = OBSTACLEINFO;
    data.datasize = sizeof(ObstacleInfo);

    for (int i = 0; i < 3; i++)
    {
        SOCKET client_sock = clientdata[i].clientsock;
        for (int j = 0; j < 3; j++)
        {
            // ObstacleInfo 전송 (고정 길이)
            retval = send(client_sock, (char*)&data, sizeof(DataInfo), 0);
            if (retval == SOCKET_ERROR) {
                error->err_display((char*)"Send_ObstacleInfo -> ObstacleInfo 전송(고정 길이)");
                return;
            }

            // ObstacleInfo 전송 (가변 길이)
            retval = send(client_sock, (char*)&obstacledata[j], data.datasize, 0);
            if (retval == SOCKET_ERROR) {
                error->err_display((char*)"Send_ObstacleInfo -> ObstacleInfo 전송(가변 길이)");
                return;
            }
        }
    }
}

void CServer::Send_CreateBullet(BulletInfo bInfo) //모든 클라이언트에 생성할 총탄 정보 send
{
    int retval;

    DataInfo data;
    data.infoindex = BULLETCREATE;
    data.datasize = sizeof(BulletInfo);

    for (int i = 0; i < 3; i++)
    {
        SOCKET client_sock = clientdata[i].clientsock;
        // BulletInfo 전송 (고정 길이)
        retval = send(client_sock, (char*)&data, sizeof(DataInfo), 0);
        if (retval == SOCKET_ERROR) {
            error->err_display((char*)"Send_BulletInfo -> BulletInfo 전송(고정 길이)");
            return;
        }

        // BulletInfo 전송 (가변 길이)
        retval = send(client_sock, (char*)&bInfo, data.datasize, 0);
        if (retval == SOCKET_ERROR) {
            error->err_display((char*)"Send_BulletInfo -> BulletInfo 전송(가변 길이)");
            return;
        }
    }
}

void CServer::Send_BulletInfo(BulletInfo bInfo) //모든 클라이언트에 총탄 정보 send
{
    int retval;

    DataInfo data;
    data.infoindex = BULLETINFO;
    data.datasize = sizeof(BulletInfo);

    for (int i = 0; i < 3; i++)
    {
        SOCKET client_sock = clientdata[i].clientsock;
        // BulletInfo 전송 (고정 길이)
        retval = send(client_sock, (char*)&data, sizeof(DataInfo), 0);
        if (retval == SOCKET_ERROR) {
            error->err_display((char*)"Send_BulletInfo -> BulletInfo 전송(고정 길이)");
            return;
        }

        // BulletInfo 전송 (가변 길이)
        retval = send(client_sock, (char*)&bInfo, data.datasize, 0);
        if (retval == SOCKET_ERROR) {
            error->err_display((char*)"Send_BulletInfo -> BulletInfo 전송(가변 길이)");
            return;
        }
    }
}

void CServer::Recv_Info(LPVOID arg) //클라이언트로부터 정보 recv
{
    int retval;

    SOCKET client_sock = (SOCKET)arg;
    DataInfo data;
    ZeroMemory(&data, sizeof(DataInfo));

    // 고정 길이
    retval = recvn(client_sock, (char*)&data, sizeof(DataInfo), 0);
    if (retval == SOCKET_ERROR) {
        error->err_display("Recv_Info -> KEY 고정 길이");
        return;
    }
    // 가변 길이
    switch (data.infoindex)
    {
    case KEYINFO:
    {
        KeyInfo Info;
        ZeroMemory(&Info, sizeof(KeyInfo));
        retval = recvn(client_sock, (char*)&Info, sizeof(KeyInfo), 0);
        if (retval == SOCKET_ERROR)
        {
            error->err_display("Recv_Info -> KEY 가변 길이");
            return;
        }
        EnterCriticalSection(&cs);
        UpdatePlayerPos(Info);
        LeaveCriticalSection(&cs);
    }
    break;
    case MOUSEINFO:
    {
        MouseInfo Info;
        ZeroMemory(&Info, sizeof(MouseInfo));
        retval = recvn(client_sock, (char*)&Info, sizeof(MouseInfo), 0);
        if (retval == SOCKET_ERROR)
        {
            error->err_display("Recv_Info -> Mouse 가변 길이");
            return;
        }
        EnterCriticalSection(&cs);
        UpdateMuzzleInfo(Info);
        if (Info.isClicked == true)
        {
            CreateBullet(Info.pIndex);
        }
        LeaveCriticalSection(&cs);
    }
    break;
    case PLAYERINFO:
        break;
    case BULLETCREATE:
        break;
    case BULLETINFO:
    {
        BulletInfo Info;
        ZeroMemory(&Info, sizeof(BulletInfo));
        retval = recvn(client_sock, (char*)&Info, sizeof(BulletInfo), 0);
        if (retval == SOCKET_ERROR)
        {
            error->err_display("Recv_Info -> BulletInfo 가변 길이");
            return;
        }
        EnterCriticalSection(&cs);
        UpdateBulletInfo(Info);
        LeaveCriticalSection(&cs);
    }
    break;
    case OBSTACLEINFO:
        break;
    case GAMESTATE:
        break;
    case PLAYERINDEX:
        break;
    default:
        break;
    }
}

int CServer::recvn(SOCKET s, char* buf, int len, int flags) // 사용자 정의 데이터 수신 함수
{
    int received;
    char* ptr = buf;
    int left = len;

    while (left > 0) {
        received = recv(s, ptr, left, flags);
        if (received == SOCKET_ERROR)
            return SOCKET_ERROR;
        else if (received == 0)
            break;
        left -= received;
        ptr += received;
    }
    return (len - left);
}

void CServer::UpdatePlayerPos(KeyInfo kInfo)    // 플레이어 좌표 계산
{
    char cIndex = kInfo.pIndex;
    if (kInfo.w == true)
    {
        if (playerdata[cIndex].y - PLAYERSPEED * CTimerManager::GetInstance()->GetDelta() > 0.f)
            playerdata[cIndex].y -= PLAYERSPEED * CTimerManager::GetInstance()->GetDelta();
    }
    if (kInfo.a == true)
    {
        if (playerdata[cIndex].x - PLAYERSPEED * CTimerManager::GetInstance()->GetDelta() > 0.f)
            playerdata[cIndex].x -= PLAYERSPEED * CTimerManager::GetInstance()->GetDelta();
    }
    if (kInfo.s == true)
    {
        if (playerdata[cIndex].y + PLAYERSPEED * CTimerManager::GetInstance()->GetDelta() < 720.f)
            playerdata[cIndex].y += PLAYERSPEED * CTimerManager::GetInstance()->GetDelta();
    }
    if (kInfo.d == true)
    {
        if (playerdata[cIndex].x + PLAYERSPEED * CTimerManager::GetInstance()->GetDelta() < 1280.f)
            playerdata[cIndex].x += PLAYERSPEED * CTimerManager::GetInstance()->GetDelta();
    }
    CollisionPlayerObstacle(cIndex);
    Send_PlayerInfo();
}

void CServer::UpdateMuzzleInfo(MouseInfo mInfo) // 총구 좌표 계산
{
    int cIndex = mInfo.pIndex;
    playerdata[cIndex].gunx1 = playerdata[cIndex].x;
    playerdata[cIndex].guny1 = playerdata[cIndex].y;

    Vector2D mouse = { mInfo.mx - playerdata[cIndex].x, mInfo.my - playerdata[cIndex].y };  // 마우스 좌표 - 플레이어 좌표 벡터
    double mousesize = sqrt(pow(mouse.x, 2) + pow(mouse.y, 2));                             // 마우스 벡터의 크기
    Vector2D nmouse = { mouse.x / mousesize, mouse.y / mousesize };                         // 마우스 단위 벡터

    playerdata[cIndex].gunx2 = nmouse.x * 50.f + playerdata[cIndex].x;
    playerdata[cIndex].guny2 = nmouse.y * 50.f + playerdata[cIndex].y;

    Send_PlayerInfo();
}

void CServer::CreateBullet(char pIndex) // 생성할 총탄 좌표 계산
{
    BulletInfo b;
    b.pIndex = pIndex;
    b.x = playerdata[pIndex].gunx2;
    b.y = playerdata[pIndex].guny2;

    b.dx = playerdata[pIndex].gunx2 - playerdata[pIndex].x;
    b.dy = playerdata[pIndex].guny2 - playerdata[pIndex].y;

    double dsize = sqrt(pow(b.dx, 2) + pow(b.dy, 2));

    b.dx /= dsize;
    b.dy /= dsize;

    Send_CreateBullet(b);
}

void CServer::UpdateBulletInfo(BulletInfo bInfo)    // 총탄 좌표 계산
{
    if (bInfo.x <= 0.f || bInfo.x >= 1280.f || bInfo.y <= 0.f || bInfo.y >= 720.f)
    {
        bInfo.isDead = true;
    }
    else
    {
        bInfo.x += bInfo.dx * BULLETSPEED * CTimerManager::GetInstance()->GetDelta();
        bInfo.y += bInfo.dy * BULLETSPEED * CTimerManager::GetInstance()->GetDelta();
    }
    CollisionBullet(bInfo);
}

void CServer::CollisionBullet(BulletInfo bInfo)
{
    if (bInfo.isDead == false)
    {
        //플레이어와 총탄 충돌
        for (int i = 0; i < 3; i++)
        {
            if (bInfo.pIndex == i)
                continue;
            if (pow(bInfo.x - playerdata[i].x, 2) + pow(bInfo.y - playerdata[i].y, 2) <= pow(BULLETRADIUS + PLAYERRADIS, 2))    //충돌하면
            {
                if (playerdata[i].nowHp <= 0.f) {
                    continue;
                }
                else {
                    playerdata[i].nowHp -= 5.f;
                    bInfo.isDead = true;
                }
            }
        }

        //장애물과 총탄 충돌
        for (int i = 0; i < 3; i++)
        {
            if (pow(bInfo.x - obstacledata[i].x, 2) + pow(bInfo.y - obstacledata[i].y, 2) <= pow(BULLETRADIUS + OBSTACLERADIUS, 2))    //충돌하면
            {
                bInfo.isDead = true;
            }
        }
    }
    Send_PlayerInfo();
    Send_BulletInfo(bInfo);
}

void CServer::CollisionPlayerObstacle(char cIndex)
{
    //플레이어와 장애물 충돌
    for (int j = 0; j < 3; j++)
    {
        if (pow(playerdata[cIndex].x - obstacledata[j].x, 2) + pow(playerdata[cIndex].y - obstacledata[j].y, 2) <= pow(PLAYERRADIS + OBSTACLERADIUS, 2))    //충돌하면
        {
            float dx = playerdata[cIndex].x - obstacledata[j].x;
            float dy = playerdata[cIndex].y - obstacledata[j].y;
            float size = sqrt(pow(dx, 2) + pow(dy, 2));
            dx /= size;
            dy /= size;

            playerdata[cIndex].x += dx * (PLAYERRADIS + OBSTACLERADIUS - size);
            playerdata[cIndex].y += dy * (PLAYERRADIS + OBSTACLERADIUS - size);
        }
    }
}