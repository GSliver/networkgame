#include "ServerMain.h"
#include "TimerManager.h"

CTimerManager* CTimerManager::m_pInstance = nullptr;

CTimerManager::CTimerManager()
	: m_fDeltaTime(0.f)
{
	QueryPerformanceCounter(&m_CurTime);
	QueryPerformanceCounter(&m_OldTime);

	QueryPerformanceFrequency(&m_CpuTick);
}


CTimerManager::~CTimerManager()
{
}

const float& CTimerManager::GetDelta() const
{
	return m_fDeltaTime;
}

void CTimerManager::UpdateTime()
{
	QueryPerformanceCounter(&m_CurTime);

	if (m_CurTime.QuadPart - m_OldTime.QuadPart > m_CpuTick.QuadPart)
	{
		QueryPerformanceCounter(&m_CurTime);
		QueryPerformanceCounter(&m_OldTime);
		QueryPerformanceFrequency(&m_CpuTick);
	}

	m_fDeltaTime = float(m_CurTime.QuadPart - m_OldTime.QuadPart) / m_CpuTick.QuadPart;
	m_OldTime.QuadPart = m_CurTime.QuadPart;
}
