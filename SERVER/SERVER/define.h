#pragma once

#define SERVERPORT 9000
#define BUFSIZE    1024

#define MAX_USER 3
#define PLAYERSPEED 100.f
#define BULLETSPEED 300.f

#define PLAYERRADIS 25.f
#define BULLETRADIUS 10.f
#define OBSTACLERADIUS 50.f

//data type
#define KEYINFO 0
#define MOUSEINFO 1
#define PLAYERINFO 2
#define BULLETCREATE 3
#define BULLETINFO 4
#define OBSTACLEINFO 5
#define GAMESTATE 6
#define PLAYERINDEX 7
#define PLAYERACCEPT 8