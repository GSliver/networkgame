#pragma once
#ifndef __TIMERMGR__
#define __TIMERMGR__

class CTimerManager
{
private:
	CTimerManager(const CTimerManager& obj);
	CTimerManager& operator=(const CTimerManager& obj);

public:
	static CTimerManager* GetInstance()
	{
		if (nullptr == m_pInstance)
		{
			m_pInstance = new CTimerManager;
		}
		return m_pInstance;
	}

	void DestroyInstance()
	{
		if (m_pInstance)
		{
			delete m_pInstance;
			m_pInstance = nullptr;
		}
	}
private:
	explicit CTimerManager();
	virtual ~CTimerManager();

public:
	const float& GetDelta() const;
public:
	void UpdateTime();

private:
	LARGE_INTEGER m_CurTime;
	LARGE_INTEGER m_OldTime;
	LARGE_INTEGER m_CpuTick;
	float m_fDeltaTime;

	static CTimerManager* m_pInstance;

};

#endif
