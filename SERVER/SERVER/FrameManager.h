#pragma once
#ifndef __FRAMEMGR__
#define __FRAMEMGR__

class CFrameManager
{
private:
	CFrameManager(const CFrameManager& obj);
	CFrameManager& operator=(const CFrameManager& obj);

public:
	static CFrameManager* GetInstance()
	{
		if (nullptr == m_pInstance)
		{
			m_pInstance = new CFrameManager;
		}
		return m_pInstance;
	}

	void DestroyInstance()
	{
		if (m_pInstance)
		{
			delete m_pInstance;
			m_pInstance = nullptr;
		}
	}

private:
	explicit CFrameManager();
	virtual ~CFrameManager();

public:
	bool FrameLimit(float fLimit);

private:
	LARGE_INTEGER m_CurTime;
	LARGE_INTEGER m_OldTime;
	LARGE_INTEGER m_CpuTick;
	float m_fTimeCount;

	static CFrameManager* m_pInstance;
};
#endif